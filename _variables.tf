variable "debug_mode" {
  description = "Toggle debug mode"
  type = bool
  default = false
}

variable "docker_image" {
  description = "Docker image without version"
  default     = "kostyaesmukov/smtp_to_telegram"
}

variable "docker_image_tag" {
  description = "Docker image version"
  type        = string
  default     = "latest"
}

variable "docker_logs_url" {
  description = "Remote logs collector address:port"
  type = string
}

variable "docker_network" {
  description = "Docker network to use for containers"
  default     = "containers"
}

variable "smtp_port" {
  description = "Port to listen for SMTP messages"
  type        = number
  default     = 2525
}

variable "telegram_chat_ids" {
  description = "Chat ID to send messages to"
  type        = list(any)
}

variable "telegram_bot_token" {
  description = "Bot Token to send messages"
  type        = string
  sensitive   = true
}
