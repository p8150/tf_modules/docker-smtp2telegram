resource "docker_image" "image" {
  name         = "${var.docker_image}:${var.docker_image_tag}"
  force_remove = true
}

resource "docker_service" "smtp2telegraf" {
  name = "smtp2telegraf"
  task_spec {
    container_spec {
      image = docker_image.image.image_id
      env = {
        "ST_TELEGRAM_CHAT_IDS"  = join(",", var.telegram_chat_ids)
        "ST_TELEGRAM_BOT_TOKEN" = var.telegram_bot_token
        #"ST_TELEGRAM_MESSAGE_TEMPLATE" = "Subject: {subject}\\n\\n{body}"
      }
      # healthcheck {
      #   test         = ["CMD", "curl", "-f", "-u", "admin:admin", "http://localhost:${local.service_port}/_cluster/health"]
      #   interval     = "1m"
      #   start_period = "5m"
      #   timeout      = "2s"
      #   retries      = 4
      # }
    }
    log_driver {
      name = var.debug_mode ? "json-file" : "fluentd"
      options = var.debug_mode ? {} : {
        fluentd-address = var.docker_logs_url
        tag             = "smtp2telegram"
      }
    }
    networks_advanced {
      name = var.docker_network
    }
    resources {
      limits {
        memory_bytes = 1024 * 1024 * 16 # 16 MB
      }
    }
    restart_policy {
      condition = "any"
      delay     = "0s"
      window    = "0s"
    }
  }
  mode {
    replicated {
      replicas = 1
    }
  }
  endpoint_spec {
    ports {
      target_port    = "2525"
      published_port = var.smtp_port
      publish_mode   = "host"
    }
  }
}